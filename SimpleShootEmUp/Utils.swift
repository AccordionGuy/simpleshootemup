//
//  Utils.swift
//  SimpleShootEmUp
//
//  Created by Jose Martin DeVilla on 1/13/16.
//  Copyright © 2016 Joey deVilla. All rights reserved.
//

import Foundation
import CoreGraphics


extension CGFloat {
  
  static func random() -> CGFloat {
    return CGFloat(Float(arc4random_uniform(UInt32.max)) / Float(UInt32.max))
  }
  
  static func random(min min: CGFloat, max: CGFloat) -> CGFloat {
    assert(min < max)
    return CGFloat.random() * (max - min) + min
  }
  
}


extension Int {
  
  static func random(min min: Int, max: Int) -> Int {
    assert(min < max)
    return Int(arc4random_uniform(UInt32(max - min + 1)) + UInt32(min))
  }
  
}