//
//  GameScene.swift
//  SimpleShootEmUp
//
//  Created by Jose Martin DeVilla on 1/13/16.
//  Copyright (c) 2016 Joey deVilla. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {

  // Ship sprite
  // -----------
  // For simplicity's sake, we'll use the spaceship that's provided in Images.xcassets
  // when you start a new Game project
  private let ship = SKSpriteNode(imageNamed: "Spaceship")
  
  var lastUpdateTime: CFTimeInterval = 0        // The last time Sprite Kit called update()
  var lastAlienSpawnTime: CFTimeInterval  = 0  // Seconds since the last alien was spawned
  
  
  override func didMoveToView(view: SKView) {
    // Set the game's background color to white
    backgroundColor = SKColor(red: 1, green: 1, blue: 1, alpha: 1)
    
    // Position the player's ship halfway across the screen,
    // near the bottom
    ship.setScale(0.15)
    ship.position = CGPoint(x: size.width / 2, y: ship.size.height * 1.25)
    addChild(ship)
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

  }
 
  override func update(currentTime: CFTimeInterval) {
    let timeSinceLastUpdate = currentTime - lastUpdateTime
    
    if currentTime - lastAlienSpawnTime > 0.5 {
      spawnAlien()
      lastAlienSpawnTime = currentTime
    }
    
    lastUpdateTime = currentTime
  }
  
  func spawnAlien() {
    
    enum Direction {
      case GoingRight
      case GoingLeft
    }
    
    let alienDirection: Direction
    let alienSpriteImage: String
    
    // Randomly pick the alien's origin
    if arc4random_uniform(2) == 0 {
      alienDirection = .GoingRight
      alienSpriteImage = "AlienGoingRight"
    }
    else {
      alienDirection = .GoingLeft
      alienSpriteImage = "AlienGoingLeft"
    }
    
    // Create the alien sprite
    let alien = SKSpriteNode(imageNamed: alienSpriteImage)
    
    // Set the alien's initial coordinates
    let alienSpawnX: CGFloat
    let alienEndX: CGFloat
    if alienDirection == .GoingRight {
      alienSpawnX = -(alien.size.width / 2)
      alienEndX = frame.size.width + (alien.size.width / 2)
    }
    else {
      alienSpawnX = frame.size.width + (alien.size.width / 2)
      alienEndX = -(alien.size.width / 2)
    }
    let minSpawnY = frame.size.height / 3
    let maxSpawnY = (frame.size.height * 0.9) - alien.size.height / 2
    let alienSpawnY = CGFloat.random(min: minSpawnY, max: maxSpawnY)
    alien.position = CGPoint(x: alienSpawnX, y: alienSpawnY)
    
    // Put the alien onscreen
    addChild(alien)
    
    // Set the alien's speed
    let minMoveTime: CGFloat = 2.0
    let maxMoveTime: CGFloat = 4.0
    let moveTime = NSTimeInterval(CGFloat.random(min: minMoveTime, max: maxMoveTime))
    
    // Send the alien on its way
    let moveAction = SKAction.moveToX(alienEndX, duration: moveTime)
    let cleanUpAction = SKAction.removeFromParent()
    alien.runAction(SKAction.sequence([moveAction, cleanUpAction]))
  }
  
}
